# hubot-paraderos-transantiago

Informa la disponibilidad de recorridos y el tiempo de llegada de un determinado paradero

See [`src/paraderos-transantiago.js`](src/paraderos-transantiago.js) for full documentation.

## Instalación

En la raíz de hubot:

`npm i -S hubot-paraderos-transantiago`

Añadir **hubot-paraderos-transantiago** a `external-scripts.json`:

```json
[
  "hubot-paraderos-transantiago"
]
```

## Uso

`hubot paradero PA14`

## NPM Module

https://www.npmjs.com/package/hubot-paraderos-transantiago
